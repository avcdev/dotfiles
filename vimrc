set nocompatible
filetype off
syntax on
filetype plugin indent on
set modelines=0
set wrap


" Tab tuff
set formatoptions=tcqrnl
set tabstop=2
set shiftwidth=2
set softtabstop=2
set expandtab
set noshiftround


" Nerd tree
map <C-t> :NERDTreeToggle<CR>

if filereadable(expand("~/.vimrc.plug"))
  source ~/.vimrc.plug
endif
colorscheme dracula
hi Normal guibg=NONE ctermbg=NONE
